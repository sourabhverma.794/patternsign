<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// User routes

Route::get('/', 'User\homeController@index')->name('home');
Route::get('/home', 'User\homeController@index');
Route::get('/products', 'User\productController@getProducts')->name('products');

// User routes