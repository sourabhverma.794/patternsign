<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Foxbrain</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/header.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css">
@yield('header_css')
</head>

<body>
    {{-- Header Section --}}
    {{-- <div class="cust-container"> --}}
    <nav class="navbar navbar-expand-lg navbar-black bg-transparent position-absolute zindex-1 container-fluid"
        style="width:100%; left:0;">
        <div class="cust-container w-100">
            <div class="row w-100 mx-0">
                <div class="col-12 col-sm-12 col-lg-2 mx-0 px-0 align-self-center">
                    <div class="row">
                        <div class="col-8 col-sm-5 col-md-5 col-lg-12 align-self-center">
                            <a class="navbar-brand mp-0 w-100" href="{{ route('home') }}">
                                <img src="{{ asset('images/logo/logo.png') }}" alt="" class="w-100" srcset="">
                            </a>
                        </div>
                        <div class="col-2 col-sm-auto col-md-auto col-lg-0 ml-auto">
                            <button class="navbar-toggler p-0" type="button" onclick="searchToggle()">
                                <i class="fa fa-search h1 font-weight-normal mb-0" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div class="col-2 col-sm-auto col-md-auto col-lg-0">
                            <button class="navbar-toggler p-0" type="button" data-bs-toggle="collapse"
                                data-bs-target="#main_nav" aria-expanded="false" aria-label="Toggle navigation">
                                <i class="fa fa-bars h1 mb-0" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-0 col-lg sub-men-cus">
                    <div class="collapse navbar-collapse" id="main_nav">
                        <ul class="navbar-nav ml-auto mr-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle " href="#" data-bs-toggle="dropdown"> Design </a>
                                <ul class="dropdown-menu shadow border-0">
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            1 </a></li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            2 </a></li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            3 </a></li>
                                    <li class="has-megasubmenu click-style">
                                        <a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            4 &raquo;
                                        </a>
                                        <div class="megasubmenu dropdown-menu shadow border-0">
                                            <div class="row">
                                                <div class="col">

                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                                <div class="col">
                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                            </div><!-- end row -->
                                        </div>
                                    </li>
                                    <li class="has-megasubmenu click-style">
                                        <a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            4 &raquo;
                                        </a>
                                        <div class="megasubmenu dropdown-menu shadow border-0">
                                            <div class="row">
                                                <div class="col">

                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                                <div class="col">
                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                            </div><!-- end row -->
                                        </div>
                                    </li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            6 </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle " href="#" data-bs-toggle="dropdown"> Premium
                                </a>
                                <ul class="dropdown-menu shadow border-0">
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            1 </a></li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            2 </a></li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            3 </a></li>
                                    <li class="has-megasubmenu click-style">
                                        <a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            4 &raquo;
                                        </a>
                                        <div class="megasubmenu dropdown-menu shadow border-0">
                                            <div class="row">
                                                <div class="col">

                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                                <div class="col">
                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                            </div><!-- end row -->
                                        </div>
                                    </li>
                                    <li class="has-megasubmenu click-style">
                                        <a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            4 &raquo;
                                        </a>
                                        <div class="megasubmenu dropdown-menu shadow border-0">
                                            <div class="row">
                                                <div class="col">

                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                                <div class="col">
                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                            </div><!-- end row -->
                                        </div>
                                    </li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            6 </a></li>
                                </ul>
                            </li>
                            <li class="nav-item d-none d-lg-block">
                                <a href="" class="nav-link">
                                    <div class="hor-line"></div>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle " href="#" data-bs-toggle="dropdown"> Fabric
                                </a>
                                <ul class="dropdown-menu shadow border-0">
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            1 </a></li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            2 </a></li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            3 </a></li>
                                    <li class="has-megasubmenu click-style">
                                        <a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            4 &raquo;
                                        </a>
                                        <div class="megasubmenu dropdown-menu shadow border-0">
                                            <div class="row">
                                                <div class="col">

                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                                <div class="col">
                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                            </div><!-- end row -->
                                        </div>
                                    </li>
                                    <li class="has-megasubmenu click-style">
                                        <a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            4 &raquo;
                                        </a>
                                        <div class="megasubmenu dropdown-menu shadow border-0">
                                            <div class="row">
                                                <div class="col">

                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                                <div class="col">
                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                            </div><!-- end row -->
                                        </div>
                                    </li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            6 </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle " href="#" data-bs-toggle="dropdown"> Wallpaper
                                </a>
                                <ul class="dropdown-menu shadow border-0">
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            1 </a></li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            2 </a></li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            3 </a></li>
                                    <li class="has-megasubmenu click-style">
                                        <a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            4 &raquo;
                                        </a>
                                        <div class="megasubmenu dropdown-menu shadow border-0">
                                            <div class="row">
                                                <div class="col">

                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                                <div class="col">
                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                            </div><!-- end row -->
                                        </div>
                                    </li>
                                    <li class="has-megasubmenu click-style">
                                        <a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            4 &raquo;
                                        </a>
                                        <div class="megasubmenu dropdown-menu shadow border-0">
                                            <div class="row">
                                                <div class="col">

                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                                <div class="col">
                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a></li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                            </div><!-- end row -->
                                        </div>
                                    </li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            6 </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle " href="#" data-bs-toggle="dropdown"> Living &
                                    Decor
                                </a>
                                <ul class="dropdown-menu shadow border-0">
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            1 </a></li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            2 </a></li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            3 </a></li>
                                    <li class="has-megasubmenu click-style">
                                        <a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            4 &raquo;
                                        </a>
                                        <div class="megasubmenu dropdown-menu shadow border-0">
                                            <div class="row">
                                                <div class="col">

                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                                <div class="col">
                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                            </div><!-- end row -->
                                        </div>
                                    </li>
                                    <li class="has-megasubmenu click-style">
                                        <a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            4 &raquo;
                                        </a>
                                        <div class="megasubmenu dropdown-menu shadow border-0">
                                            <div class="row">
                                                <div class="col">

                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                                <div class="col">
                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                            </div><!-- end row -->
                                        </div>
                                    </li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            6 </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle " href="#" data-bs-toggle="dropdown"> Dining
                                </a>
                                <ul class="dropdown-menu shadow border-0">
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            1 </a></li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            2 </a></li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            3 </a></li>
                                    <li class="has-megasubmenu click-style">
                                        <a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            4 &raquo;
                                        </a>
                                        <div class="megasubmenu dropdown-menu shadow border-0">
                                            <div class="row">
                                                <div class="col">

                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                                <div class="col">
                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                            </div><!-- end row -->
                                        </div>
                                    </li>
                                    <li class="has-megasubmenu click-style">
                                        <a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            4 &raquo;
                                        </a>
                                        <div class="megasubmenu dropdown-menu shadow border-0">
                                            <div class="row">
                                                <div class="col">

                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                                <div class="col">
                                                    <ul class="list-unstyled">
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                        <li class="my-3"><a class="text-gray"
                                                                href="{{ route('products') }}">Custom Menu</a>
                                                        </li>
                                                    </ul>
                                                </div><!-- end col-3 -->
                                            </div><!-- end row -->
                                        </div>
                                    </li>
                                    <li><a class="dropdown-item click-style text-black h6 py-2"
                                            href="{{ route('products') }}"> Dropdown item
                                            6 </a></li>
                                </ul>
                            </li>

                            {{-- <li class="nav-item"><a class="nav-link" href="#"> Services </a></li> --}}
                        </ul>

                        <ul class="navbar-nav">
                            <li class="nav-item dropdown d-none d-md-block">
                                <a class="nav-link  dropdown-toggle " href="#" onclick="searchToggle()"> Search <i
                                        class="fa fa-search h4 ml-2 my-0" aria-hidden="true"></i></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle " href="#" data-bs-toggle="dropdown"> Het Pattel
                                </a>
                                <ul class="dropdown-menu shadow border-0 user-lg-card pr-2">
                                    <li><a class="dropdown-item click-style text-black font-s4 py-3 click-style"
                                            href="#">
                                            <svg class="mr-2" width="33" height="37" viewBox="0 0 36 37"
                                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M18 33.0026C21.8191 33.0026 25.4818 31.4643 28.1823 28.7259C30.8829 25.9876 32.4 22.2736 32.4 18.401C32.4 14.5284 30.8829 10.8144 28.1823 8.07607C25.4818 5.33772 21.8191 3.79934 18 3.79934C14.1809 3.79934 10.5182 5.33772 7.81766 8.07607C5.11714 10.8144 3.6 14.5284 3.6 18.401C3.6 22.2736 5.11714 25.9876 7.81766 28.7259C10.5182 31.4643 14.1809 33.0026 18 33.0026ZM18 36.6531C8.0586 36.6531 0 28.4816 0 18.401C0 8.32038 8.0586 0.148926 18 0.148926C27.9414 0.148926 36 8.32038 36 18.401C36 28.4816 27.9414 36.6531 18 36.6531Z"
                                                    fill="black" />
                                                <path
                                                    d="M18.3664 19.5442C18.3841 19.5442 18.4019 19.5442 18.4232 19.5442C18.4302 19.5442 18.4373 19.5442 18.4444 19.5442C18.4551 19.5442 18.4693 19.5442 18.4799 19.5442C19.5193 19.5253 20.3601 19.136 20.9809 18.3914C22.3466 16.751 22.1196 13.9389 22.0947 13.6706C22.0061 11.656 21.1121 10.6922 20.3742 10.2424C19.8244 9.90602 19.1823 9.72459 18.4657 9.70947H18.4409C18.4373 9.70947 18.4302 9.70947 18.4267 9.70947H18.4054C18.0117 9.70947 17.2383 9.77751 16.4969 10.2273C15.7519 10.6771 14.8438 11.6409 14.7551 13.6706C14.7303 13.9389 14.5032 16.751 15.869 18.3914C16.4863 19.136 17.327 19.5253 18.3664 19.5442ZM15.7023 13.7651C15.7023 13.7537 15.7058 13.7424 15.7058 13.7348C15.8229 11.0248 17.6285 10.7338 18.4019 10.7338H18.4161C18.4232 10.7338 18.4338 10.7338 18.4444 10.7338C19.4022 10.7564 21.0305 11.1722 21.1405 13.7348C21.1405 13.7462 21.1405 13.7575 21.144 13.7651C21.1476 13.7915 21.3959 16.3617 20.2678 17.7148C19.8208 18.2515 19.2249 18.5161 18.4409 18.5237C18.4338 18.5237 18.4302 18.5237 18.4232 18.5237C18.4161 18.5237 18.4125 18.5237 18.4054 18.5237C17.625 18.5161 17.0255 18.2515 16.582 17.7148C15.4575 16.3693 15.6987 13.7877 15.7023 13.7651Z"
                                                    fill="black" stroke="black" />
                                                <path
                                                    d="M25.7135 24.2081C25.7135 24.2043 25.7135 24.2005 25.7135 24.1967C25.7135 24.1665 25.7099 24.1363 25.7099 24.1022C25.6887 23.3539 25.6425 21.6039 24.1029 21.0445C24.0923 21.0407 24.0781 21.0369 24.0675 21.0331C22.4676 20.5985 21.1372 19.6158 21.1231 19.6044C20.9067 19.4419 20.6087 19.4986 20.4561 19.7291C20.3036 19.9597 20.3568 20.2772 20.5732 20.4397C20.6335 20.4851 22.0454 21.5321 23.812 22.0159C24.6386 22.3296 24.7308 23.2707 24.7557 24.1325C24.7557 24.1665 24.7557 24.1967 24.7592 24.227C24.7628 24.5671 24.7415 25.0925 24.6847 25.3949C24.11 25.7426 21.8574 26.9446 18.4305 26.9446C15.0179 26.9446 12.751 25.7388 12.1728 25.3911C12.116 25.0887 12.0912 24.5634 12.0983 24.2232C12.0983 24.193 12.1018 24.1627 12.1018 24.1287C12.1267 23.2669 12.2189 22.3258 13.0455 22.0121C14.8121 21.5283 16.224 20.4775 16.2843 20.4359C16.5007 20.2734 16.5539 19.9559 16.4014 19.7254C16.2488 19.4948 15.9509 19.4381 15.7345 19.6006C15.7203 19.612 14.3971 20.5947 12.7901 21.0294C12.7759 21.0331 12.7652 21.0369 12.7546 21.0407C11.215 21.6039 11.1689 23.3539 11.1476 24.0985C11.1476 24.1325 11.1476 24.1627 11.144 24.193C11.144 24.1967 11.144 24.2005 11.144 24.2043C11.1405 24.4008 11.1369 25.41 11.325 25.9165C11.3604 26.0148 11.4243 26.0979 11.5094 26.1546C11.6158 26.2302 14.1665 27.9613 18.4341 27.9613C22.7017 27.9613 25.2523 26.2264 25.3587 26.1546C25.4403 26.0979 25.5077 26.0148 25.5432 25.9165C25.7206 25.4138 25.717 24.4046 25.7135 24.2081Z"
                                                    fill="black" stroke="black" />
                                            </svg>
                                            Profile </a></li>
                                    <li><a class="dropdown-item click-style text-black font-s4 py-3 click-style"
                                            href="#">
                                            <svg class="mr-2" width="33" height="27" viewBox="0 0 36 27"
                                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M30.4992 11.6587C29.7126 5.17694 24.4062 0.148926 18 0.148926C13.0392 0.148926 8.73 3.18268 6.7626 7.96399C2.8962 9.17298 0 12.992 0 17.0973C0 22.2891 4.0374 26.513 9 26.513H10.8V22.7467H9C6.0228 22.7467 3.6 20.212 3.6 17.0973C3.6 14.4533 5.7582 11.9054 8.4114 11.4177L9.4572 11.2256L9.8028 10.1748C11.0682 6.31247 14.2092 3.91522 18 3.91522C22.9626 3.91522 27 8.13913 27 13.331V15.2141H28.8C30.7854 15.2141 32.4 16.9033 32.4 18.9804C32.4 21.0575 30.7854 22.7467 28.8 22.7467H25.2V26.513H28.8C32.7708 26.513 36 23.1346 36 18.9804C35.9979 17.2922 35.4548 15.6535 34.4576 14.3262C33.4604 12.9989 32.0667 12.0597 30.4992 11.6587Z"
                                                    fill="black" />
                                                <path
                                                    d="M19.7998 17.097V9.56445H16.1998V17.097H10.7998L17.9998 26.5128L25.1998 17.097H19.7998Z"
                                                    fill="black" />
                                            </svg>
                                            Download History </a>
                                    </li>
                                    <li><a class="dropdown-item click-style text-black font-s4 py-3 click-style"
                                            href="#">
                                            <svg class="mr-2" width="33" height="29" viewBox="0 0 33 29"
                                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M32.2016 5.96732C31.6896 4.82873 30.9513 3.79695 30.0281 2.92974C29.1042 2.05994 28.0149 1.36872 26.8194 0.893674C25.5797 0.399123 24.2502 0.145984 22.9078 0.148952C21.0246 0.148952 19.1873 0.64421 17.5906 1.5797C17.2086 1.80348 16.8457 2.04928 16.5019 2.31708C16.1581 2.04928 15.7952 1.80348 15.4132 1.5797C13.8165 0.64421 11.9791 0.148952 10.0959 0.148952C8.73989 0.148952 7.42585 0.398415 6.18439 0.893674C4.98495 1.37059 3.90392 2.05661 2.97569 2.92974C2.05126 3.79597 1.31281 4.82799 0.802175 5.96732C0.271212 7.15227 0 8.41059 0 9.7056C0 10.9272 0.259752 12.2002 0.775436 13.4952C1.20708 14.5775 1.8259 15.7001 2.61662 16.8337C3.86954 18.6276 5.59231 20.4986 7.73144 22.3952C11.2763 25.5392 14.7868 27.711 14.9357 27.799L15.8411 28.3567C16.2421 28.6025 16.7578 28.6025 17.1589 28.3567L18.0642 27.799C18.2132 27.7073 21.7198 25.5392 25.2685 22.3952C27.4077 20.4986 29.1304 18.6276 30.3833 16.8337C31.1741 15.7001 31.7967 14.5775 32.2245 13.4952C32.7402 12.2002 33 10.9272 33 9.7056C33.0038 8.41059 32.7326 7.15227 32.2016 5.96732ZM16.5019 25.4548C16.5019 25.4548 2.90311 17.0868 2.90311 9.7056C2.90311 5.96732 6.12327 2.93707 10.0959 2.93707C12.8883 2.93707 15.3101 4.43385 16.5019 6.62033C17.6937 4.43385 20.1155 2.93707 22.9078 2.93707C26.8805 2.93707 30.1007 5.96732 30.1007 9.7056C30.1007 17.0868 16.5019 25.4548 16.5019 25.4548Z"
                                                    fill="black" />
                                            </svg>
                                            Favorite </a></li>
                                    <li class="has-megasubmenu click-style"><a
                                            class="dropdown-item click-style text-black font-s4 py-3 click-style"
                                            href="#">
                                            <svg class="mr-2" version="1.1" id="Capa_1"
                                                xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="36"
                                                height="27" viewBox="0 0 902.86 902.86"
                                                style="enable-background:new 0 0 902.86 902.86;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path
                                                            d="M671.504,577.829l110.485-432.609H902.86v-68H729.174L703.128,179.2L0,178.697l74.753,399.129h596.751V577.829z
                                                         M685.766,247.188l-67.077,262.64H131.199L81.928,246.756L685.766,247.188z" />
                                                        <path d="M578.418,825.641c59.961,0,108.743-48.783,108.743-108.744s-48.782-108.742-108.743-108.742H168.717
                                                        c-59.961,0-108.744,48.781-108.744,108.742s48.782,108.744,108.744,108.744c59.962,0,108.743-48.783,108.743-108.744
                                                        c0-14.4-2.821-28.152-7.927-40.742h208.069c-5.107,12.59-7.928,26.342-7.928,40.742
                                                        C469.675,776.858,518.457,825.641,578.418,825.641z M209.46,716.897c0,22.467-18.277,40.744-40.743,40.744
                                                        c-22.466,0-40.744-18.277-40.744-40.744c0-22.465,18.277-40.742,40.744-40.742C191.183,676.155,209.46,694.432,209.46,716.897z
                                                         M619.162,716.897c0,22.467-18.277,40.744-40.743,40.744s-40.743-18.277-40.743-40.744c0-22.465,18.277-40.742,40.743-40.742
                                                        S619.162,694.432,619.162,716.897z" />
                                                    </g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                            </svg>
                                            Cart</a></li>
                                    <li class="has-megasubmenu click-style"><a
                                            class="dropdown-item click-style text-black font-s4 py-3 click-style"
                                            href="#">
                                            <svg class="mr-2" width="27" height="31" viewBox="0 0 31 31"
                                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M30.3503 15.2069L20.4504 6.84135V11.8607H8.90038V18.5531H20.4504V23.5724L30.3503 15.2069ZM3.95039 3.49514H17.1504V0.148926H3.95039C2.13539 0.148926 0.650391 1.65472 0.650391 3.49514V26.9186C0.650391 28.759 2.13539 30.2648 3.95039 30.2648H17.1504V26.9186H3.95039V3.49514Z"
                                                    fill="black" />
                                            </svg>
                                            Sign Out</a></li>
                                </ul>
                            </li>
                            <li class="nav-item d-none d-md-block"><a
                                    class="nav-link btn btn-black text-white py-1-5 px-3 rounded-10 w-max" href="#">
                                    Buy now
                                </a>
                            </li>
                        </ul>
                    </div> <!-- navbar-collapse.// -->
                </div>
            </div>
        </div> <!-- container-fluid.// -->
        <div class="container-fluid w-100 mt-3 search-cus d-none" id="seachCus">
            <div class="row w-100 mx-0">
                <div class="col-12 col-md-10 mx-auto">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control border-black rounded-10"
                            style="padding:12px; height:100%;" placeholder="Search" aria-label="Recipient's username"
                            aria-describedby="button-addon2">
                        <button class="btn btn-black text-white btn-lg rounded-10" type="button" id="button-addon2"><i
                                class="fa fa-search h4 mb-0 mx-1 mx-md-5" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    {{-- Header Section --}}

    {{-- Body Section --}}
    @yield('public_content')
    {{-- Body Section --}}

    {{-- Footer Section --}}
    </div>
    <div class="container-fluid mp-0 pt-5 py-4"
        style="background-image: url('{{ asset('images/Rectangle 1470.png') }}'); background-size:cover;">
        <div class="cust-container px-0 mx-0">
            {{-- <img src="{{ asset('images/Rectangle 1470.png') }}" class="d-block w-100 position-absolute" alt="..."> --}}
            <div class="row mx-10">
                <div class="col-12 col-md-6">
                    <a class="navbar-brand mx-0" href="#" >
                        <img src="{{ asset('images/logo/logo.png') }}" alt="" class="w-100" srcset="">
                    </a>
                </div>
                <div class="col-12 col-md-6 mt-3 mt-md-auto">
                    <div class="input-group mb-3 border rounded-pill border-black">
                        <input type="text" class="form-control bg-transparent border-0 place-black"
                            placeholder="Your E-mail" aria-label="Recipient's username" aria-describedby="button-addon2"
                            style="padding: 5px 0px 0px 12px;">
                        <button class="btn btn-black rounded-pill text-white px-2 px-md-5" type="button"
                            id="button-addon2">Subscribe Now!</button>
                    </div>
                </div>
            </div>
            <div class="row mx-10 my-5">
                <div class="col-12 col-md-4">
                    <p class=" text-justify">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Turpis adipiscing sed sollicitudin
                        dolor
                        nisl sed magna lectus vestibulum. Tempor, eget scelerisque pulvinar habitasse purus commodo.
                        Viverra
                        ridiculus lorem id luctus. Euismod augue nunc, scelerisque sagittis tempus etiam ut.Lorem ipsum
                        dolor sit amet, consectetur adipiscing elit. Turpis adipiscing sed sollicitudin dolor nisl sed
                        magna
                        lectus vestibulum. Tempor, eget scelerisque pulvinar habitasse purus commodo. Viverra ridiculus
                        lorem id luctus. Euismod augue nunc, scelerisque sagittis tempus etiam ut.
                    </p>
                </div>
                <div class="col-12 col-md-8">
                    <div class="row">
                        <div class="col-12 col-md-3 ml-auto">
                            <p class="h5 font-weight-bold">EXPLORE</p>
                            <ul class="h6 font-weight-normal ft-ul">
                                <li><a href="#" class="text-gray">Fabric</a></li>
                                <li><a href="#" class="text-gray">Wallpaper</a></li>
                                <li><a href="#" class="text-gray">Living & Decor</a></li>
                                <li><a href="#" class="text-gray">Dining</a></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-3">
                            <p class="h5 font-weight-bold">COMPANY</p>
                            <ul class="h6 font-weight-normal ft-ul">
                                <li><a href="#" class="text-gray">About Us</a></li>
                                <li><a href="#" class="text-gray">Career</a></li>
                                <li><a href="#" class="text-gray">Blog</a></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-3">
                            <p class="h5 font-weight-bold">SUPPORT</p>
                            <ul class="h6 font-weight-normal ft-ul">
                                <li><a href="tel:+919825647896" class="text-gray"> +91 9825647896 </a></li>
                                <li><a href="#" class="text-gray">Contact Us</a></li>
                                <li><a href="#" class="text-gray">FAQ</a></li>
                                <li>
                                    <a href="http://" class="text-black font-s3 mx-1"><i class="fa fa-instagram"
                                            aria-hidden="true"></i></a>
                                    <a href="http://" class="text-black font-s3 mx-1"><i
                                            class="fa fa-facebook-official" aria-hidden="true"></i></a>
                                    <a href="http://" class="text-black font-s3 mx-1"><i
                                            class="fa fa-pinterest-square" aria-hidden="true"></i></a>
                                    <a href="http://" class="text-black font-s3 mx-1"><i class="fa fa-twitter-square"
                                            aria-hidden="true"></i></a>
                                    <a href="http://" class="text-black font-s3 mx-1"><i class="fa fa-linkedin"
                                            aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row border-black w-100 p-0 m-0"></div>
            <div class="row mx-10 mt-4">
                <div class="col-12 col-md-6">
                    <p class="h5 mx-auto mr-md-auto ml-md-0" style="width:fit-content;">© 2009-2021 Patternsign, Inc.
                    </p>
                </div>
                <div class="col-auto mx-auto col-md-6">
                    <div class="dropdown">
                        <button
                            class="btn btn-transparent border-black rounded-10 dropdown-toggle  text-left pull-right"
                            style="width: 200px;" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            English <i class="fa fa-caret-down pull-right" style="line-height: 22px;"
                                aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item click-style" href="#">English</a></li>
                            <li><a class="dropdown-item click-style" href="#">English</a></li>
                            <li><a class="dropdown-item click-style" href="#">English</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Footer Section --}}

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/header.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>

    <script>
        function searchToggle() {
            var x = document.getElementById("seachCus").classList;
            if (x.contains("d-none")) {
                x.remove('d-none');
            } else {
                x.add('d-none');
            }
        }
    </script>
</body>

</html>
