@extends('user.header')
@section('header_css')
    <style>
        .navbar {
            position: relative !important;
            box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.2);
        }

        #main {
            transition: margin-left .5s;
            padding: 16px;
        }

    </style>
@endsection
@section('public_content')

    <div class="container-fluid mp-0 cust-container">
        <div class="row mp-0 my-5 position-relative">
            <div id="mySidenav" class="sidenav py-0">
                <div class="container-fluid side-cust-con">
                    <div class="row border-bottom pb-4 d-block text-center">
                        {{-- <div class="col-6 text-center"> --}}
                        <button class="btn btn-black btn-sm p-2 mt-1 w-auto" onclick="closeNav()">
                            <svg class="mr-3" width="38" height="20" viewBox="0 0 48 30" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M9 12H39V18H9V12ZM0 0H48V6H0V0ZM18 24H30V30H18V24Z" fill="white" />
                            </svg>
                            Hide</button>
                        {{-- </div>
                        <div class="col-6 text-center"> --}}
                        <button class="btn btn-white btn-sm mt-1 border-black p-2 w-auto" onclick="resetFilter()">
                            <svg class="mr-3" width="38" height="20" viewBox="0 0 54 30" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M6.02804 17.9439V12.0561H47.8037V17.9439H6.02804ZM0 30V23.972H41.9159V30H0ZM11.9159 0H53.8318V6.02804H11.9159V0Z"
                                    fill="black" />
                            </svg>
                            Clear</button>
                        {{-- </div> --}}
                    </div>
                    <div class="row border-bottom py-3">
                        <div class="col px-0">
                            <h4>Sort By</h4>
                            <ul class="list-group">
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        First checkbox
                                    </label>

                                </li>
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        Second checkbox
                                    </label>

                                </li>
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        Third checkbox
                                    </label>

                                </li>
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        Fourth checkbox
                                    </label>

                                </li>
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        Fifth checkbox
                                    </label>

                                </li>
                            </ul>
                            <a class="text-black cust-but-undl font-weight-light pull-right pr-0 pl-2"
                                style="font-size:12px;">Apply</a>
                        </div>
                    </div>
                    <div class="row border-bottom py-3">
                        <div class="col px-0">
                            <h4>Products</h4>
                            <ul class="list-group">
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        First checkbox
                                    </label>

                                </li>
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        Second checkbox
                                    </label>

                                </li>
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        Third checkbox
                                    </label>

                                </li>
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        Fourth checkbox
                                    </label>

                                </li>
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        Fifth checkbox
                                    </label>

                                </li>
                            </ul>
                            <a class="text-black cust-but-undl font-weight-light pull-right pr-0 pl-2"
                                style="font-size:12px;">Apply</a>
                        </div>
                    </div>
                    <div class="row border-bottom py-3">
                        <div class="col px-0">
                            <h4>Colors</h4>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-auto p-2">
                                        <div class="card w-100 border-0">
                                            <input type="checkbox" name="pickFilColor[]" value="black" id="sel-col-1"
                                                class="h-in-cb">
                                            <span onclick="pickFilColor(this)" for="sel-col-1"
                                                class="fil-col-patch rounded-5 bg-black"></span>
                                            {{-- <img src="" alt=""> --}}
                                        </div>
                                    </div>
                                    <div class="col-auto p-2">
                                        <div class="card w-100 border-0">
                                            <input type="checkbox" name="pickFilColor[]" value="blue" id="sel-col-2"
                                                class="h-in-cb">
                                            <span onclick="pickFilColor(this)" for="sel-col-2"
                                                class="fil-col-patch rounded-5 bg-blue"></span>
                                            {{-- <img src="" alt=""> --}}
                                        </div>
                                    </div>
                                    <div class="col-auto p-2">
                                        <div class="card w-100 border-0">
                                            <input type="checkbox" name="pickFilColor[]" value="green" id="sel-col-3"
                                                class="h-in-cb">
                                            <span onclick="pickFilColor(this)" for="sel-col-3"
                                                class="fil-col-patch rounded-5 bg-green"></span>

                                            {{-- <img src="" alt=""> --}}
                                        </div>
                                    </div>
                                    <div class="col-auto p-2">
                                        <div class="card w-100 border-0">
                                            <input type="checkbox" name="pickFilColor[]" value="pink" id="sel-col-4"
                                                class="h-in-cb">
                                            <span onclick="pickFilColor(this)" for="sel-col-4"
                                                class="fil-col-patch rounded-5 bg-pink"></span>
                                            {{-- <img src="" alt=""> --}}
                                        </div>
                                    </div>
                                    <div class="col-auto p-2">
                                        <div class="card w-100 border-0">
                                            <input type="checkbox" name="pickFilColor[]" value="brown" id="sel-col-5"
                                                class="h-in-cb">
                                            <span onclick="pickFilColor(this)" for="sel-col-5"
                                                class="fil-col-patch rounded-5 bg-brown"></span>
                                            {{-- <img src="" alt=""> --}}
                                        </div>
                                    </div>
                                    <div class="col-auto p-2">
                                        <div class="card w-100 border-0">
                                            <input type="checkbox" name="pickFilColor[]" value="blue" id="sel-col-6"
                                                class="h-in-cb">
                                            <span onclick="pickFilColor(this)" for="sel-col-6"
                                                class="fil-col-patch rounded-5 bg-blue"></span>
                                            {{-- <img src="" alt=""> --}}
                                        </div>
                                    </div>
                                    <div class="col-auto p-2">
                                        <div class="card w-100 border-0">
                                            <input type="checkbox" name="pickFilColor[]" value="green" id="sel-col-7"
                                                class="h-in-cb">
                                            <span onclick="pickFilColor(this)" for="sel-col-7"
                                                class="fil-col-patch rounded-5 bg-green"></span>
                                            {{-- <img src="" alt=""> --}}
                                        </div>
                                    </div>
                                    <div class="col-auto p-2">
                                        <div class="card w-100 border-0">
                                            <input type="checkbox" name="pickFilColor[]" value="pink" id="sel-col-8"
                                                class="h-in-cb">
                                            <span onclick="pickFilColor(this)" for="sel-col-8"
                                                class="fil-col-patch rounded-5 bg-pink"></span>
                                            {{-- <img src="" alt=""> --}}
                                        </div>
                                    </div>
                                    <div class="col-auto p-2">
                                        <div class="card w-100 border-0">
                                            <input type="checkbox" name="pickFilColor[]" value="brown" id="sel-col-9"
                                                class="h-in-cb">
                                            <span onclick="pickFilColor(this)" for="sel-col-9"
                                                class="fil-col-patch rounded-5 bg-brown"></span>
                                            {{-- <img src="" alt=""> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="text-black cust-but-undl font-weight-light pull-right pr-0 pl-2"
                                style="font-size:12px;">Apply</a>
                        </div>
                    </div>
                    <div class="row border-bottom py-3">
                        <div class="col px-0">
                            <h4>Category</h4>
                            <ul class="list-group">
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        First checkbox
                                    </label>

                                </li>
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        Second checkbox
                                    </label>

                                </li>
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        Third checkbox
                                    </label>

                                </li>
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        Fourth checkbox
                                    </label>

                                </li>
                                <li class="list-group-item border-0 d-flex align-items-center">
                                    <label class="checkbox path d-flex mb-0 mr-2 mr-md-3">
                                        <input type="checkbox" class="mr-2">
                                        <svg viewBox="0 0 21 21">
                                            <path
                                                d="M5,10.75 L8.5,14.25 L19.4,2.3 C18.8333333,1.43333333 18.0333333,1 17,1 L4,1 C2.35,1 1,2.35 1,4 L1,17 C1,18.65 2.35,20 4,20 L17,20 C18.65,20 20,18.65 20,17 L20,7.99769186">
                                            </path>
                                        </svg>
                                        Fifth checkbox
                                    </label>

                                </li>
                            </ul>
                            <a class="text-black cust-but-undl font-weight-light pull-right pr-0 pl-2"
                                style="font-size:12px;">Apply</a>
                        </div>
                    </div>
                </div>
                <div class="container-fluid side-cust-con">
                    <div class="row py-3 d-block text-center">
                        <button class="btn btn-white font-s4 font-weight-normal mt-1 border-black px-2 px-md-4 w-auto"
                            onclick="resetFilter()">
                            Clear Filters</button>
                    </div>
                </div>
            </div>
            <div class="col" id="main">
                <a class="text-black cust-but-undl h5 font-weight-light" onclick="openNav()">Filter</a>
                <h1>Flower</h1>
                <div class="row">
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p1.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p2.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p3.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p4.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p5.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p6.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p7.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p8.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p9.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p10.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p11.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p12.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p1.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p2.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p3.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 px-2 py-4">
                        <div class="card border-0 shadow pb-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                            <img src="{{ asset('images/products/p4.png') }}" class="card-img-top" alt="...">
                            <div class="card-body p-0 w-100" style="text-align-last: left;">
                                <p class="card-title text-center mb-0 font-s5 mt-2 pl-2">Flower Patter</p>
                                <p class="card-title text-center mb-0 font-s5 mt-1 pl-2">10 January 2021</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-auto ml-auto d-flex">
                        <button class="btn btn-gray"><i class="fa fa-chevron-left"></i></button>
                        <ul class="mb-0 mx-1">
                            <li><a class="btn btn-white border-black">1</a></li>
                        </ul>
                        <button class="btn btn-gray"><i class="fa fa-chevron-right"></i></button>
                        <p class="mb-0 font-s4 align-self-center ml-2">of 2</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function openNav() {
            $("#mySidenav").addClass('custMySidenav');
            $("#main").addClass('custMain');
            $(".side-cust-con").delay(300).fadeIn();
        }

        function closeNav() {
            $(".side-cust-con").fadeOut(100);
            $("#mySidenav").removeClass("custMySidenav");
            $("#main").removeClass('custMain');
        }

        function pickFilColor(e) {
            // console.log(document.getElementById(e.getAttribute('for')).checked);

            if (document.getElementById(e.getAttribute('for')).checked == false) {
                var checkbox = document.getElementsByName("pickFilColor");
                for (var i = 0; i < checkbox.length; i++) {
                    checkbox[i].checked = false;
                }
                var items = document.getElementsByClassName("fil-col-patch");
                // console.log(items);
                for (var i = 0; i < items.length; i++) {
                    items[i].classList.remove('fil-col-patch-active');
                }
                document.getElementById(e.getAttribute('for')).checked = true;
                document.getElementById(e.getAttribute('for')).value;
                e.classList.add('fil-col-patch-active');
            } else {
                document.getElementById(e.getAttribute('for')).checked = false;
                e.classList.remove('fil-col-patch-active');
            }
            // console.log(document.getElementById(e.getAttribute('for')).checked);

        }

        function resetFilter() {
            // console.log("oh");
            var items = document.querySelectorAll("[type='checkbox']");
            // console.log(items);
            for (var i = 0; i < items.length; i++) {
                // console.log(items[i]);
                items[i].checked = false;
            }
            var patch = document.getElementsByClassName("fil-col-patch");
            // console.log(patch);
            for (var i = 0; i < patch.length; i++) {
                patch[i].classList.remove('fil-col-patch-active');
            }
        }
    </script>

@endsection
