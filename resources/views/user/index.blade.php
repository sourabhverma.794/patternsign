@extends('user.header')
@section('public_content')
    <div class="container-fluid mp-0">
        <div class="row mp-0">
            <div id="carouselExampleSlidesOnly" style="padding:0px;" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{ asset('images/Rectangle 1470.png') }}" style="min-height:290px;" class="d-block w-100" alt="...">
                        <div class="carousel-caption zindex-0">
                            <h1 class="font-s1">Take your audience on a visual journey</h1>
                            <p class="font-s3">Discover authentic, story-driven photos, videos, and illustrations that
                                have the power to move, inspire—and engage.</p>
                            <button class="btn btn-white rounded-10 px-10 py-2">Buy Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- </div> --}}
    <div class="cust-container">
        <div class="container-fluid mp-0">
            <div class="row mp-0 my-5">
                <div class="col-12">
                    <div class="row">
                        <p class="text-center font-s2 fw-7">NEW ARRIVALS</p>
                    </div>
                    <div class="row ">
                        <div class="col-6 col-md-6 col-lg-3 p-3">
                            <img src="{{ asset('images/products/p1.png') }}" class="img-fluid w-100 mx-auto d-block" alt="...">
                        </div>
                        <div class="col-6 col-md-6 col-lg-3 p-3">
                            <img src="{{ asset('images/products/p2.png') }}" class="img-fluid w-100 mx-auto d-block" alt="...">
                        </div>
                        <div class="col-6 col-md-6 col-lg-3 p-3">
                            <img src="{{ asset('images/products/p3.png') }}" class="img-fluid w-100 mx-auto d-block" alt="...">
                        </div>
                        <div class="col-6 col-md-6 col-lg-3 p-3">
                            <img src="{{ asset('images/products/p4.png') }}" class="img-fluid w-100 mx-auto d-block" alt="...">
                        </div>
                        <div class="col-6 col-md-6 col-lg-3 p-3">
                            <img src="{{ asset('images/products/p5.png') }}" class="img-fluid w-100 mx-auto d-block" alt="...">
                        </div>
                        <div class="col-6 col-md-6 col-lg-3 p-3">
                            <img src="{{ asset('images/products/p6.png') }}" class="img-fluid w-100 mx-auto d-block" alt="...">
                        </div>
                        <div class="col-6 col-md-6 col-lg-3 p-3">
                            <img src="{{ asset('images/products/p7.png') }}" class="img-fluid w-100 mx-auto d-block" alt="...">
                        </div>
                        <div class="col-6 col-md-6 col-lg-3 p-3">
                            <img src="{{ asset('images/products/p8.png') }}" class="img-fluid w-100 mx-auto d-block" alt="...">
                        </div>
                        <div class="col-6 col-md-6 col-lg-3 p-3">
                            <img src="{{ asset('images/products/p9.png') }}" class="img-fluid w-100 mx-auto d-block" alt="...">
                        </div>
                        <div class="col-6 col-md-6 col-lg-3 p-3">
                            <img src="{{ asset('images/products/p10.png') }}" class="img-fluid w-100 mx-auto d-block"
                                alt="...">
                        </div>
                        <div class="col-6 col-md-6 col-lg-3 p-3">
                            <img src="{{ asset('images/products/p11.png') }}" class="img-fluid w-100 mx-auto d-block"
                                alt="...">
                        </div>
                        <div class="col-6 col-md-6 col-lg-3 p-3">
                            <img src="{{ asset('images/products/p12.png') }}" class="img-fluid w-100 mx-auto d-block"
                                alt="...">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-auto mx-auto">
                            <a class="btn btn-neavy text-white px-5 fw-5 font-s4" href="{{ route('products') }}">See More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my-5 mx-0">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-md-6 r3 ">
                            <div class="row position-absolute w-100 r3-i1">
                                <svg width="101" height="101" class="r3-sv1" viewBox="0 0 101 101" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="3.3334" cy="3.92715" r="2.8334" stroke="black" />
                                    <circle cx="26.6664" cy="3.92715" r="2.8334" stroke="black" />
                                    <circle cx="50.0009" cy="3.92715" r="2.8334" stroke="black" />
                                    <circle cx="73.3339" cy="3.92715" r="2.8334" stroke="black" />
                                    <circle cx="96.6679" cy="3.92715" r="2.8334" stroke="black" />
                                    <circle cx="3.3334" cy="27.2614" r="2.8334" stroke="black" />
                                    <circle cx="26.6664" cy="27.2594" r="2.8334" stroke="black" />
                                    <circle cx="50.0009" cy="27.2594" r="2.8334" stroke="black" />
                                    <circle cx="73.3339" cy="27.2594" r="2.8334" stroke="black" />
                                    <circle cx="96.6679" cy="27.2594" r="2.8334" stroke="black" />
                                    <circle cx="3.3334" cy="50.5934" r="2.8334" stroke="black" />
                                    <circle cx="26.6664" cy="50.5937" r="2.8334" stroke="black" />
                                    <circle cx="50.0009" cy="50.5937" r="2.8334" stroke="black" />
                                    <circle cx="73.3339" cy="50.5937" r="2.8334" stroke="black" />
                                    <circle cx="96.6679" cy="50.5937" r="2.8334" stroke="black" />
                                    <circle cx="3.3334" cy="73.9281" r="2.8334" stroke="black" />
                                    <circle cx="26.6664" cy="73.9281" r="2.8334" stroke="black" />
                                    <circle cx="50.0009" cy="73.9281" r="2.8334" stroke="black" />
                                    <circle cx="73.3339" cy="73.9281" r="2.8334" stroke="black" />
                                    <circle cx="96.6679" cy="73.9281" r="2.8334" stroke="black" />
                                    <circle cx="3.3334" cy="97.2604" r="2.8334" stroke="black" />
                                    <circle cx="26.6664" cy="97.2606" r="2.8334" stroke="black" />
                                    <circle cx="50.0009" cy="97.2606" r="2.8334" stroke="black" />
                                    <circle cx="73.3339" cy="97.2606" r="2.8334" stroke="black" />
                                    <circle cx="96.6679" cy="97.2606" r="2.8334" stroke="black" />
                                </svg>
                                <div class="col-1"></div>
                                <div class="col-7">
                                    <img src="{{ asset('images/products/p1.png') }}" class="w-100" alt="...">
                                </div>
                            </div>
                            <div class="row position-absolute w-100 r3-i2">
                                <div class="col-7 ml-auto">
                                    <img src="{{ asset('images/products/p2.png') }}" class="w-100" alt="...">
                                </div>
                                <div class="col-1"></div>
                                <svg width="101" class="r3-sv2" height="101" viewBox="0 0 101 101" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="3.3334" cy="3.92715" r="2.8334" stroke="black" />
                                    <circle cx="26.6664" cy="3.92715" r="2.8334" stroke="black" />
                                    <circle cx="50.0009" cy="3.92715" r="2.8334" stroke="black" />
                                    <circle cx="73.3339" cy="3.92715" r="2.8334" stroke="black" />
                                    <circle cx="96.6679" cy="3.92715" r="2.8334" stroke="black" />
                                    <circle cx="3.3334" cy="27.2614" r="2.8334" stroke="black" />
                                    <circle cx="26.6664" cy="27.2594" r="2.8334" stroke="black" />
                                    <circle cx="50.0009" cy="27.2594" r="2.8334" stroke="black" />
                                    <circle cx="73.3339" cy="27.2594" r="2.8334" stroke="black" />
                                    <circle cx="96.6679" cy="27.2594" r="2.8334" stroke="black" />
                                    <circle cx="3.3334" cy="50.5934" r="2.8334" stroke="black" />
                                    <circle cx="26.6664" cy="50.5937" r="2.8334" stroke="black" />
                                    <circle cx="50.0009" cy="50.5937" r="2.8334" stroke="black" />
                                    <circle cx="73.3339" cy="50.5937" r="2.8334" stroke="black" />
                                    <circle cx="96.6679" cy="50.5937" r="2.8334" stroke="black" />
                                    <circle cx="3.3334" cy="73.9281" r="2.8334" stroke="black" />
                                    <circle cx="26.6664" cy="73.9281" r="2.8334" stroke="black" />
                                    <circle cx="50.0009" cy="73.9281" r="2.8334" stroke="black" />
                                    <circle cx="73.3339" cy="73.9281" r="2.8334" stroke="black" />
                                    <circle cx="96.6679" cy="73.9281" r="2.8334" stroke="black" />
                                    <circle cx="3.3334" cy="97.2604" r="2.8334" stroke="black" />
                                    <circle cx="26.6664" cy="97.2606" r="2.8334" stroke="black" />
                                    <circle cx="50.0009" cy="97.2606" r="2.8334" stroke="black" />
                                    <circle cx="73.3339" cy="97.2606" r="2.8334" stroke="black" />
                                    <circle cx="96.6679" cy="97.2606" r="2.8334" stroke="black" />
                                </svg>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 align-self-center justify-content-center">
                            <p class="text-left font-s2 fw-5">Lorem Ipsum</p>
                            <p class="font-s4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod tempor
                                incididunt ut labore et
                                dolore magna aliqua. Ullamcorper sit amet risus nullam eget. Dictum sit amet justo donec
                                enim diam
                                vulputate.</p>
                            <p class="font-s4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod tempor
                                incididunt ut labore et
                                dolore magna aliqua. Ullamcorper sit amet risus nullam eget. Dictum sit amet justo donec
                                enim diam
                                vulputate.</p>
                            <p class="font-s4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                eiusmod tempor
                                incididunt ut labore et
                                dolore magna aliqua. Ullamcorper sit amet risus nullam eget. Dictum sit amet justo donec
                                enim diam
                                vulputate.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mp-0 my-5">
                <div class="col-12">
                    <div class="row">
                        <p class="text-center font-s2 fw-7">Choose Preminum Collection To Browse</p>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-6 col-md-2 text-center p-2 p-md-3">
                            <a href="{{ route('products') }}" class="btn btn-white-in w-100 py-3 rounded-10 font-s4">Fabric</a>
                        </div>
                        <div class="col-6 col-md-2 text-center p-2 p-md-3">
                            <a href="{{ route('products') }}" class="btn btn-white-in w-100 py-3 rounded-10 font-s4">Wallpaper</a>
                        </div>
                        <div class="col-6 col-md-2 text-center p-2 p-md-3">
                            <a href="{{ route('products') }}" class="btn btn-white-in w-100 py-3 rounded-10 font-s4">Project</a>
                        </div>
                        <div class="col-6 col-md-2 text-center p-2 p-md-3">
                            <a href="{{ route('products') }}" class="btn btn-white-in w-100 py-3 rounded-10 font-s4">Holidays</a>
                        </div>
                        <div class="col-6 col-md-2 text-center p-2 p-md-3">
                            <a href="{{ route('products') }}" class="btn btn-white-in w-100 py-3 rounded-10 font-s4">Trending</a>
                        </div>
                        <div class="col-0 d-none d-md-block col-md-12"></div>
                        <div class="col-6 col-md-2 text-center p-2 p-md-3">
                            <a href="{{ route('products') }}" class="btn btn-white-in w-100 py-3 rounded-10 font-s4">Animals</a>
                        </div>
                        <div class="col-6 col-md-2 text-center p-2 p-md-3">
                            <a href="{{ route('products') }}" class="btn btn-white-in w-100 py-3 rounded-10 font-s4">Beach</a>
                        </div>
                        <div class="col-6 col-md-2 text-center p-2 p-md-3">
                            <a href="{{ route('products') }}" class="btn btn-white-in w-100 py-3 rounded-10 font-s4">Nature</a>
                        </div>
                        <div class="col-6 col-md-2 text-center p-2 p-md-3">
                            <a href="{{ route('products') }}" class="btn btn-white-in w-100 py-3 rounded-10 font-s4">Tea Towel</a>
                        </div>
                        <div class="col-6 col-md-2 text-center p-2 p-md-3">
                            <a href="{{ route('products') }}" class="btn btn-white-in w-100 py-3 rounded-10 font-s4">Skull</a>
                        </div>
                    </div>
                    <div class="row my-5">
                        <div class="col-auto mx-auto">
                            <a class="btn btn-neavy text-white px-5 fw-5 font-s4" href="{{ route('products') }}">See More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mp-0 my-5">
                <div class="col-12">
                    <div class="row">
                        <p class="text-center font-s2 fw-7">FEATURED CATEGORIES</p>
                    </div>
                    <div class="row">
                        <div class="col-6 col-md-3">
                            <div class="card border-0 py-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                                <img src="{{ asset('images/products/p1.png') }}" class="card-img-top shadow rounded-10" alt="...">
                                <div class="card-body p-0 w-100 align-self-center">
                                    {{-- <h5 class="card-title text-center font-s4">Home Decore</h5> --}}
                                    <a href="{{ route('products') }}" class="btn btn-white-in rounded-10 px-5 mt-3 mt-md-5 font-s4 w-100">Home Decore</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="card border-0 py-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                                <img src="{{ asset('images/products/p2.png') }}" class="card-img-top shadow rounded-10" alt="...">
                                <div class="card-body p-0 w-100 align-self-center">
                                    {{-- <h5 class="card-title text-center font-s4">Menswear</h5> --}}
                                    <a href="{{ route('products') }}" class="btn btn-white-in rounded-10 px-5 mt-3 mt-md-5 font-s4 w-100">Menswear</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="card border-0 py-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                                <img src="{{ asset('images/products/p3.png') }}" class="card-img-top shadow rounded-10" alt="...">
                                <div class="card-body p-0 w-100 align-self-center">
                                    {{-- <h5 class="card-title text-center font-s4">Womenswear</h5> --}}
                                    <a href="{{ route('products') }}" class="btn btn-white-in rounded-10 px-5 mt-3 mt-md-5 font-s4 w-100">Womenswear</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="card border-0 py-3 py-md-auto p-0 mx-auto w-100" style="width: 18rem;">
                                <img src="{{ asset('images/products/p4.png') }}" class="card-img-top shadow rounded-10" alt="...">
                                <div class="card-body p-0 w-100 align-self-center">
                                    {{-- <h5 class="card-title text-center font-s4">Bags</h5> --}}
                                    <a href="{{ route('products') }}" class="btn btn-white-in rounded-10 px-5 mt-3 mt-md-5 font-s4 w-100">Bags</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection
